package com.nla;

import java.time.ZonedDateTime;

public interface IntervalCalculator {


    ZonedDateTime getBeginDate();

    ZonedDateTime getEndDate();

    default boolean isInInterval(ZonedDateTime intervalBeginDate, ZonedDateTime intervalEndDate) {

        if(intervalBeginDate.isAfter(getEndDate()) || intervalEndDate.isBefore(getBeginDate())) {
            return false;
        }

        if(intervalEndDate.isAfter(getBeginDate())) {
            return true;
        }


        throw new IllegalStateException("unknown");

    }


}

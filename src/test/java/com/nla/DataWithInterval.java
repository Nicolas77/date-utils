package com.nla;

import java.time.ZonedDateTime;

public class DataWithInterval implements IntervalCalculator{

    private ZonedDateTime beginDate;
    private ZonedDateTime endDate;

    public DataWithInterval(ZonedDateTime beginDate, ZonedDateTime endDate) {
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public ZonedDateTime getBeginDate() {
        return this.beginDate;
    }

    public ZonedDateTime getEndDate() {
        return this.endDate;
    }
}

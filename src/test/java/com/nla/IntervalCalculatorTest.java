package com.nla;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IntervalCalculatorTest {

    private DataWithInterval data;

    @Before
    public void before() {
        ZonedDateTime begin = ZonedDateTime.of(LocalDateTime.of(2018, Month.OCTOBER, 1, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        ZonedDateTime end = ZonedDateTime.of(LocalDateTime.of(2018, Month.OCTOBER, 4, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        data = new DataWithInterval(begin, end);
    }

    @Test
    public void beginBeforeAndFinishBeforeIntervalTest() {
        ZonedDateTime beginInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.OCTOBER, 5, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        ZonedDateTime endInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.OCTOBER, 6, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        boolean res = data.isInInterval(beginInterval, endInterval);
        assertFalse(res);
    }

    @Test
    public void beginBeforeAndFinishDuringInterval() {
        ZonedDateTime beginInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.OCTOBER, 2, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        ZonedDateTime endInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.OCTOBER, 6, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        boolean res = data.isInInterval(beginInterval, endInterval);
        assertTrue(res);
    }

    @Test
    public void beginBeforeAndFinishAfterInterval() {
        ZonedDateTime beginInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.OCTOBER, 2, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        ZonedDateTime endInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.OCTOBER, 3, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        boolean res = data.isInInterval(beginInterval, endInterval);
        assertTrue(res);
    }

    @Test
    public void beginDuringAndFinishDuringInterval() {
        ZonedDateTime beginInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.SEPTEMBER, 30, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        ZonedDateTime endInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.OCTOBER, 5, 18,00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        boolean res = data.isInInterval(beginInterval, endInterval);
        assertTrue(res);
    }


    @Test
    public void beginDuringAndFinishAfterInterval() {
        ZonedDateTime beginInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.SEPTEMBER, 30, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        ZonedDateTime endInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.OCTOBER, 3, 18,00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        boolean res = data.isInInterval(beginInterval, endInterval);
        assertTrue(res);
    }

    @Test
    public void beginAfterAndFinishAfterInterval() {
        ZonedDateTime beginInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.SEPTEMBER, 1, 18, 00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        ZonedDateTime endInterval = ZonedDateTime.of(LocalDateTime.of(2018, Month.SEPTEMBER, 3, 18,00).truncatedTo(ChronoUnit.MINUTES), ZoneId.systemDefault());
        boolean res = data.isInInterval(beginInterval, endInterval);
        assertFalse(res);
    }



}
